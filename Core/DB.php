<?php


/**
 * Class DB
 */
abstract class DB
{
    /**
     * @var PDO
     */
    protected $db;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        try {
            $this->db = new PDO('mysql:host=ok309080.mysql.tools;dbname=ok309080_db',
                            'ok309080_db',
                            'e4auhlmg');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec('SET NAMES "utf8"');
        } catch (Exception $e) {
            $message = 'DB access error! ' . $e->getMessage();
            die($message);
        }
    }
}