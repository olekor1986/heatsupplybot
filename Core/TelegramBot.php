<?php


/**
 * Class TelegramBot
 */
class TelegramBot
{

    /**
     * @var string
     */
    protected $token = '1025369841:AAGFU1y8xo9OTiCupaz4_5PSv9zZQycfjP8';
    /**
     * @var array
     */
    protected $data;

    /**
     * TelegramBot constructor.
     */
    public function __construct() {
        $this->data = [];
        $this->data = json_decode(file_get_contents('php://input'), true);
        $this->selectMessage();
    }

    /**
     *  Selected message to answer...
     */
    private function selectMessage()
    {
        $text = $this->data['message']['text'];
        if (!empty($text)){
            if ($text == '/start') {
                $message = 'Вас приветствует HeatSupplyBot!' . "\r\n"
                    . 'Бот создан для дополнительного мониторинга параметров котельных.' . "\r\n"
                    . 'Введите /help для получения списка команд.';
            } else if ($text == '/help') {
                $message = 'Список доступных команд:' . "\r\n"
                    . '/param - вывести последние параметры.' . "\r\n"
                    . '/param:число - вывести последние (число) записей параметров, по умолчанию 5, но не более 50.' . "\r\n"
                    . '/draw:число - возвращает линейный график в виде изображения *.jpg, где (число) - количество точек графика.' . "\r\n";
            } else if ($text == '/param') {
                $heat_source_1 = new Model('Приморская,59', 'monitoring_prim59', 'date');
                $message = $heat_source_1->getLastData();
            } else if (preg_match('/^(\/param:)\d{1,2}$/', $text)) {
                $limit = intval(substr(strstr($text, ':'), 1 ));
                $heat_source_1 = new Model('Приморская,59', 'monitoring_prim59', 'date');
                $heat_source_1->setLimit($limit);
                $message = $heat_source_1->getLastDataByLimit();
            } else if (preg_match('/^(\/draw:)\d{1,2}$/', $text)){
                $limit = intval(substr(strstr($text, ':'), 1 ));
                $heat_source_1 = new Model('Приморская,59', 'monitoring_prim59', 'date');
                $heat_source_1->setLimit($limit);
                $drawPath = new ChartDraw($heat_source_1, 500,500);
                $message = $drawPath->DrawLinearChart($heat_source_1->getDataByLimit());
            } else {
                $message = 'Введите команду или посмотрите список доступных команд /help';
            }
        }
        $tg_id = $this->data['message']['chat']['id'];
        $this->send($tg_id, $message);
    }

    /**
     * @param string $id
     * @param string $message
     * @return mixed
     */
    public function send(string $id, string $message) {

        $data = array(
            'chat_id'      => $id,
            'text'     => $message,
        );

        $out = $this->request('sendMessage', $data);
        return $out;
    }


    /**
     * @param string $method
     * @param array $data
     * @return mixed
     */
    public  function request(string $method, array $data) {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot' . $this->token .  '/' . $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $out = json_decode(curl_exec($curl), true);

        curl_close($curl);

        return $out;
    }
}